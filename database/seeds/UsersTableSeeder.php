<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
        	[ 
                'first_name' => 'Peter', 'last_name' => 'Harris',
                'email' => 'peteharris401@gmail.com', 'password' => 'peteharris401', 
                'company' => 'HarrisTech', 'mobile_no' => '08055998877', 
                'type' => 'vendor'
            ]
        ];

        User::truncate();
        collect( $users )->each( function ( $user ) {
        	User::create( $user );
        });
    }
}
