<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id', 'created_at', 'updated_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Hash the password automatically 
     *
     * @access public
     * @param string $value 
     * @return void 
     */
    public function setPasswordAttribute( $value = '' )
    {
        $this->attributes['password'] = bcrypt( $value );
    }

    /**
     * Get the name attribute 
     *
     * @access public
     * @return string 
     */
    public function getNameAttribute()
    {
        return "{$this->attributes['last_name']} {$this->attributes['first_name']}";
    }
}
