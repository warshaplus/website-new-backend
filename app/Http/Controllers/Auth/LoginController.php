<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        // authenticate the user with the JWT auth package 
        $validation = validator( $request->all(), ['email' => 'required|string|email', 'password' => 'required|string']);

        if ( $validation->fails() ) {
            return response()->json(['success' => false, 'details' => $validation->errors()->first()]);
        }

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return response()->json(['success' => false, 'details' => 'Too many login attempt. Please try again later']);
        }

        // get the user 
        $user = User::where('email', $request->email)->first();

        if ( ! $user ) {
            $this->incrementLoginAttempts($request);
            return response()->json(['success' => false, 'details' => 'No user found with the provided details.']);
        }

        // make sure the passwords match 
        if ( ! Hash::check( $request->password, $user->password ) ) {
            // passwords do not match 
            $this->incrementLoginAttempts($request);
            return response()->json(['success' => false, 'details' => 'No user found with the provided details.']);
        }

        // we have the valid user 
        try {
            if ( ! $token = JWTAuth::fromUser( $user ) ) {
                return $this->sendFailedLoginResponse( $request );
            }

            // we have the token 
            $success = true;
            return response()->json( compact('token', 'user', 'success') );

        } catch ( JWTException $e ) {
            info( $e );
            return response()->json([
                'success' => false, 'details' => 'There was an error while loggin in', 'error' => $e->getMessage()
            ]);   
        }
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        return response()->json(['success' => true, 'user' => $user]);
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        return response()->json(['success' => false, 'details' => trans('auth.failed')]);
    }
}
