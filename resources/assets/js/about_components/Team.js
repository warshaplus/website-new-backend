import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Row, Col } from 'react-bootstrap';
import { FaFacebookOfficial, FaTwitter, FaInstagram  } from 'react-icons/lib/fa';

class Team extends Component {
    render() {
        return (
            <div>
                <br />
                <div className="container">
                    <div style={title}>
                        <h2 className="divider-style"><span>Meet the team</span></h2>
                    </div>
                </div>
                <div className="team-clean">
                    <div className="container">
                        <Row className="people">
                            <Col md={4} sm={6} className="item">
                                <img src="1.jpg" className="img-circle" />
                                <h3 className="name">Ben Johnson</h3>
                                <p className="title">Musician</p>
                                <p className="description">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est, et interdum justo suscipit id. Etiam dictum feugiat tellus, a semper massa. </p>
                                <div className="social">
                                    <a href="#"><FaFacebookOfficial /></a>
                                    <a href="#"><FaTwitter /></a>
                                    <a href="#"><FaInstagram /></a>
                                </div>
                            </Col>
                            <Col md={4} sm={6} className="item">
                                <img src="2.jpg" className="img-circle" />
                                <h3 className="name">Emily Clark</h3>
                                <p className="title">Artist</p>
                                <p className="description">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est, et interdum justo suscipit id. Etiam dictum feugiat tellus, a semper massa. </p>
                                <div className="social">
                                    <a href="#"><FaFacebookOfficial /></a>
                                    <a href="#"><FaTwitter /></a>
                                    <a href="#"><FaInstagram /></a>
                                </div>
                            </Col>
                            <Col md={4} sm={6} className="item">
                                <img src="3.jpg" className="img-circle" />
                                <h3 className="name">Carl Kent</h3>
                                <p className="title">Stylist</p>
                                <p className="description">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est, et interdum justo suscipit id. Etiam dictum feugiat tellus, a semper massa. </p>
                                <div className="social">
                                    <a href="#"><FaFacebookOfficial /></a>
                                    <a href="#"><FaTwitter /></a>
                                    <a href="#"><FaInstagram /></a>
                                </div>
                            </Col>
                        </Row>
                    </div>
                </div>
            </div>
        );
    }
}

const title = {
    textAlign: 'center'
}

export default Team;