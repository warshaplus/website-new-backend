import React from 'react'
import {
  Redirect, Route
} from 'react-router-dom'

import AuthService from './AuthService'

const AuthenticatedWrapper = ({ component: Component, ...rest }) => (
  <Route {...rest} render={ props => (
    ( new AuthService ).isAuthenticated() ? (
      <Component {...props}/>
    ) : (
      <Redirect to={{
        pathname: '/login',
        state: { from: props.location }
      }}/>
    )
  )}/>
)

export default AuthenticatedWrapper