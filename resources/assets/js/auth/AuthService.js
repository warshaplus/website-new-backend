const LS_USERDATA = "LS_USERDATA"
const LS_USERTOKEN = "LS_USERTOKEN"

export default class AuthService {
	constructor() {
		// 
	}

	/**
	 * Check if the user is authenticated
	 * @return {Boolean}
	 */
	isAuthenticated() {
		return this.getUser() != null
	}

	/**
	 * Set the user data in localStorage so we can retain it accross refreshes 
	 * @param {Object} userData  
	 * @param {String} userToken
	 */
	setUserData( userData, userToken ) {
		userData.name = userData.last_name + ' ' + userData.first_name 
		
		localStorage.setItem( LS_USERDATA, JSON.stringify( userData ) )
		localStorage.setItem( LS_USERTOKEN, JSON.stringify( userToken ) )
	}

	/**
	 * Get the user 
	 * @return {Object} 
	 */
	getUser() {
		return JSON.parse( localStorage.getItem( LS_USERDATA ) )
	}

	/**
	 * Get the user's token from localStorage
	 * @return {string} 
	 */
	getToken() {
		return JSON.parse( localStorage.getItem( LS_USERTOKEN ) )
	}

	/**
	 * Clear the user data from local storate
	 * @return {voud} 
	 */
	clearUserData() {
		localStorage.removeItem( LS_USERDATA )
		localStorage.removeItem( LS_USERTOKEN )
	}
}