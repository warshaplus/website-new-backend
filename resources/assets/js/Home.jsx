import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Button } from 'semantic-ui-react';

//CSS
require('../css/style.scss');

//Home components
import Latest_orders from './home_components/Latest_orders';
import What_we_do from './home_components/What_we_do';
import MapContainer from './home_components/Map';

//Header && Footer
import Header from './header_footer/Header';
import Footer from './header_footer/Footer';


class Home extends Component {
    render() {
        return (
            <div>
                {/* -- ( Header ) -- */}
                <Header />
                
                {/* -- ( Latest Orders ) -- */}
                <Latest_orders />

                {/* -- ( what we do ) -- */}
                <What_we_do />

                {/* -- ( Google Map API ) -- */}
                <MapContainer />

                {/* -- ( Footer ) -- */}
                <Footer />
            </div>
        );
    }
}
export default Home;