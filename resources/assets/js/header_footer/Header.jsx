import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Navbar, Nav, Col, Row, NavbarBrand } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import { Button } from 'semantic-ui-react';
import $ from 'jquery';
import AuthService from '../auth/AuthService';
import logo2 from '../../../imgs/logo2.png';

class Header extends Component {

    constructor(props) {
        super(props);

        this.state = { user: null }
    
        this.doLogout = this.doLogout.bind( this )
        this.authService = new AuthService
    }

    componentDidMount() {
        this.setState({
            user: this.authService.getUser()
        });
    }

    doLogout() {
        this.authService.clearUserData()
        window.location = '/login'
    }

    

    render() {
        return (
    <div className="header-dark">
        <Navbar className="navbar navbar-default">
            <div className="container">
                <Navbar.Header className="navbar-header">
                    <NavbarBrand>
                        <img src={logo2} />
                    </NavbarBrand>
                    <Navbar.Toggle />
                </Navbar.Header>
                <Navbar.Collapse className="navbar-collapse">
                    {/* -- Left nav -- */}
                    <ul className="nav navbar-nav">
                        <li><NavLink className="header_link" to="/"> Home </NavLink></li>
                        <li><NavLink className="header_link" to="/about"> About </NavLink></li>
                        <li><NavLink className="header_link" to="faq"> FAQs </NavLink></li>
                    </ul>

                    {/* -- Right nav -- */}
                    <Nav className="navbar-text navbar-right p">
                        { ! this.state.user && <li><NavLink className="navbar-link login" to="/login"> Login </NavLink></li> }
                        { ! this.state.user && <li><NavLink className="btn btn-default action-button" to="/register" style={{backgroundColor: '#FF8844'}}> Create an account </NavLink></li> }

                        { this.state.user && <li><NavLink className="header_link" to="/admin-dashboard"> Admin Dashboard </NavLink></li> }
                        { this.state.user && <li>
                            <a className="header_link" onClick={ this.doLogout }>
                                <span><FaUser /></span> Logout ({ this.state.user.name })
                            </a>
                        </li>  }
                    </Nav>
                </Navbar.Collapse>
            </div>
        </Navbar>

        <div className="container hero">
            <Row>
                <center><h1><strong> Warshaplus <br /> We are the best</strong></h1></center>
                <br />
                <center><Button color='orange' size='huge' > Order Now </Button></center>
            </Row>
        </div>
    </div>
        )
    }
}
export default Header;