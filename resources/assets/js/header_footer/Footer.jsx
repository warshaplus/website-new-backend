import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Row, Col } from 'react-bootstrap';
import { FaMapMarker, FaPhone, FaEnvelope, FaFacebook, FaTwitter, FaLinkedin } from 'react-icons/lib/fa';
import { Link } from 'react-router-dom';
import logo from '../../../imgs/logo.png';


class Footer extends Component {
    render() {
        return (
            <footer>
                <Row>
                    <Col md={4} mdOffset={0} sm={6} className="footer-navigation">
                        <h3><img src={logo} /></h3>
                        <p className="links">
                            <Link to="/">Home</Link><strong> · </strong>
                            <Link to="/about">About</Link><strong> · </strong>
                            <Link to="/faq">FAQs </Link><strong> · </strong>
                            <Link to="/terms-&-conditions">Terms & Conditions</Link><strong> · </strong>
                            <Link to="/privacy-policy">Privacy Policy </Link><strong>  </strong>
                        </p>
                        <p className="company-name">Warshaplus © 2017 - 2018</p>
                    </Col>
                    <Col md={3} sm={6} className="footer-contacts">
                        <div><span className="fa footer-contacts-icon"><FaMapMarker /></span>
                            <p><span className="new-line-span">Salman building</span> Jeddah, Saudi Arabia</p>
                        </div>
                        <div><i className="fa footer-contacts-icon"><FaPhone /></i>
                            <p className="footer-center-info email text-left"><a href="tel:+966590005427">+966-590005427</a></p>
                        </div>
                        <div><i className="fa footer-contacts-icon"><FaEnvelope /></i>
                            <p> <a href="mailto:info@warshaplus.com">info@warshaplus.com</a></p>
                        </div>
                    </Col>
                    <div className="clearfix visible-sm-block"></div>
                    <Col md={4} className="footer-about">
                        <h3><strong> About Warshaplus </strong></h3>
                        <p>
                            Lorem ipsum dolor sit amet, consectateur adispicing elit. Fusce euismod convallis velit, eu auctor lacus vehicula sit amet.
                            <Link to="/about" style={{fontSize: '12px'}}> continue reading >> </Link>
                        </p>
                        <div className="social-links social-icons">
                            <a href="https://www.facebook.com/warshaplus/" target="_blank"><FaFacebook /></a>
                            <a href="https://twitter.com/warshaplus?lang=en" target="_blank"><FaTwitter /></a>
                            <a href="https://www.linkedin.com/company/%D9%88%D8%B1%D8%B4%D8%A9-%D8%A8%D9%84%D8%B3-warshaplus" target="_blank"><FaLinkedin /></a>
                        </div>
                    </Col>
                    <div className="clearfix"></div>
                </Row>
            </footer>
        )
    }
}
export default Footer;