import React from 'react';
import { Card, CardText, CardHeader } from 'material-ui/Card';
import { ViewTitle } from 'admin-on-rest/lib/mui';
import { Responsive } from 'admin-on-rest';

export default () => (
        <Responsive
            small={
                <Card>
                    <ViewTitle title="Dashboard" />
                    <CardText>Lorem ipsum sic dolor amet...</CardText>
                </Card>
            }
            medium={
                <Card>
                    <CardHeader title="Dashboard" />
                    <CardText>Lorem ipsum sic dolor amet...</CardText>
                </Card>
            }
        />
);