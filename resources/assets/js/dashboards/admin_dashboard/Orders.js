import React from 'react';
import { List, Datagrid, TextField, Responsive, SimpleList, ReferenceField } from 'admin-on-rest';

export const OrderList = (props) => (
    <List title="All Orders" {...props}>
        <Responsive
        small={
            <SimpleList
                primaryText={record => record.title}
                secondaryText={record => `${record.views} views`}
                tertiaryText={record => new Date(record.published_at).toLocaleDateString()}
            />
        }
        medium={
            <Datagrid>
                <TextField source="id" />
                <ReferenceField label="User" source="userId" reference="users">
                    <TextField source="name" />
                </ReferenceField>
                <TextField source="title" />
                <TextField source="body" />
            </Datagrid>
        }
    />
    </List>
);

export default OrderList;