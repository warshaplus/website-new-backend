import React from 'react';
import { List, Datagrid, TextField } from 'admin-on-rest';

export const VendorList = (props) => (
    <List title="All Vendors" {...props}>
        <Datagrid>
            <TextField source="id" />
            <TextField source="name" />
            <TextField source="email" />
            <TextField source="password" />
            <TextField source="company" />
            <TextField source="phone" />
    </Datagrid>
    </List>
);

export default VendorList;