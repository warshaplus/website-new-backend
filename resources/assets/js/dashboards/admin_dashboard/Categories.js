import React from 'react';
import { List, Datagrid, TextField } from 'admin-on-rest';

export const CategoryList = (props) => (
    <List title="Categories" {...props}>
        <Datagrid>
            <TextField source="id" />
            <TextField source="name" />
            <TextField source="email" />
            <TextField source="password" />
            <TextField source="phone" />
        </Datagrid>
    </List>
);

export default CategoryList;