import React from 'react';
import { List, Datagrid, TextField } from 'admin-on-rest';

export const AdminSettings = (props) => (
    <List title="Your Settings" {...props}>
        <Datagrid>
            <TextField source="id" />
            <TextField source="name" />
            <TextField source="email" />
            <TextField source="password" />
            <TextField source="phone" />
        </Datagrid>
    </List>
);

export default AdminSettings;