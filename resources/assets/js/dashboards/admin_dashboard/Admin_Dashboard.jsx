import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { jsonServerRestClient, Admin, Resource, Delete } from 'admin-on-rest';
import authClient from './authClient';
import { MdSettings, MdGroup, MdPerson, MdDirectionsCar, MdShoppingBasket,
         MdUnarchive, MdAttachMoney, MdLayers, MdStyle, MdRateReview, MdDashboard } from 'react-icons/lib/md';

//Lists
import Dashboard from './Dashboard';
import OrderList from './Orders';
import CustomerList from './Customers';
import VendorList from './Vendors';
import CarList from './Cars';
import PartList from './Parts';
import AdminSettings from './Settings';
import TransactionList from './Payments';
import CategoryList from './Categories';
import ReviewList from './Reviews';
import SegmentList from './Segments';


export default class Admin_Dashboard extends React.Component {
    constructor() {
        super();
        this.state = {
            adminName: "Naif"
        }
    }

    render() {
        return (
            <Admin dashboard={Dashboard} title={"Welcome " + this.state.adminName} authClient={authClient} restClient={jsonServerRestClient('http://jsonplaceholder.typicode.com')}>
                <Resource name="orders" list={OrderList} icon={MdShoppingBasket} />
                <Resource name="customers" list={CustomerList} icon={MdGroup} />
                <Resource name="vendors" list={VendorList} icon={MdPerson} />
                <Resource name="cars" list={CarList} icon={MdDirectionsCar} />
                <Resource name="parts Resources" list={PartList} icon={MdUnarchive} />
                <Resource name="transactions" list={TransactionList} icon={MdAttachMoney} />
                <Resource name="categories" list={CategoryList} icon={MdStyle} />
                <Resource name="reviews" list={ReviewList} icon={MdRateReview} />
                <Resource name="segments" list={SegmentList} icon={MdLayers} />
                <Resource name="settings" list={AdminSettings} icon={MdSettings} />
            </Admin>
        )
    }
}