import React from 'react';
import { List, Datagrid, TextField } from 'admin-on-rest';

export const SegmentList = (props) => (
    <List title="Segments" {...props}>
        <Datagrid>
            <TextField source="id" />
            <TextField source="name" />
            <TextField source="email" />
            <TextField source="password" />
            <TextField source="phone" />
        </Datagrid>
    </List>
);

export default SegmentList;