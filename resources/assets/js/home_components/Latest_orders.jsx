import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Card } from 'semantic-ui-react';
import { Row, Col } from 'react-bootstrap';

class Latest_orders extends Component {
    render() {
        return (
            <div className="card-wrapper">
                <div className="choose-feed visible">
                    <ul>
                        <li>
                            Latest Orders
                            <span className="triangle-down"></span>
                        </li>
                    </ul>
                </div>
                <br />
                <br />
                {/* Cards */}
                <center>
                    <div className="container">
                        <Row>
                            <Col sm={4}>
                                <Card>
                                    <span>
                                        <img src="https://blogmedia.dealerfire.com/wp-content/uploads/sites/275/2017/11/New-Custom-Ford-F-Series-Trucks-from-SEMA-2017_o.jpg" alt="Ford"/>
                                        Fusion
                                    </span>
                                    <Card.Content description>
                                        <h2> Car piece </h2>
                                    </Card.Content>
                                    <Card.Content extra>
                                        <Col md={6}>
                                            1min ago
                                        </Col>
                                        <Col md={6}>
                                            <span className="stars"> &#9734; &#9734; &#9734; &#9734; &#9734; </span>
                                        </Col>
                                    </Card.Content>
                                </Card>
                            </Col>
                            <Col sm={4}>
                                <Card>
                                    <span>
                                        <img src="http://www.carlogos.org/logo/Ferrari-emblem-1920x1080.png" alt="Ferrari"/>
                                        488 gtb spider
                                    </span>
                                    <Card.Content description>
                                        <h2> Car piece </h2>
                                    </Card.Content>
                                    <Card.Content extra>
                                        <Col md={6}>
                                            9hours ago
                                        </Col>
                                        <Col md={6}>
                                            <span className="stars"> &#9734; &#9734; &#9734; &#9734; &#9734; </span>
                                        </Col>
                                    </Card.Content>
                                </Card>
                            </Col>
                            <Col sm={4}>
                                <Card>
                                    <span>
                                        <img src="https://seeklogo.com/images/T/toyota-logo-BE11A14C6B-seeklogo.com.png" alt="Toyota"/>
                                        Camry
                                    </span>
                                    <Card.Content description>
                                        <h2> Car piece </h2>
                                    </Card.Content>
                                    <Card.Content extra>
                                        <Col md={6}>
                                            1/1/2018 at 12:30PM
                                        </Col>
                                        <Col md={6}>
                                            <span className="stars"> &#9734; &#9734; &#9734; &#9734; &#9734; </span>
                                        </Col>
                                    </Card.Content>
                                </Card>
                            </Col>
                        </Row>
                        </div>
                    </center>
                </div>
        );
    }
}
export default Latest_orders;