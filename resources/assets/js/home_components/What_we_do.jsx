import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Row, Col } from 'react-bootstrap';

//Imgs
import truck from '../../../imgs/truck.jpg';
import deliver from '../../../imgs/deliver.png';
import quality from '../../../imgs/quality.png';

class What_we_do extends Component {
    render() {
        return (
            <center>
                <div className="container boxes">
                <br />
                    <Row>
                        <Col md={4} sm={6} className="box">
                            <span>
                                <img src={truck} width="200" height="100"/>
                                <h2> we blah </h2>
                                <p>blah blah blah</p>
                            </span>
                        </Col>
                        <Col md={4} sm={6} className="box">
                            <span>
                                <img src={deliver} width="100" height="100"/>
                                <h2> we blah </h2>
                                <p>blah blah blah</p>
                            </span>
                        </Col>
                        <Col md={4} sm={6} className="box">
                            <span>
                                <img src={quality} width="200" height="100"/>
                                <h2> we blah </h2>
                                <p>blah blah blah</p>
                            </span>
                        </Col>
                    </Row>
                </div>
            </center>
        );
    }
}
export default What_we_do;