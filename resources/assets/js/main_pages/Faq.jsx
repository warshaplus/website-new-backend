import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { PanelGroup, Panel } from 'react-bootstrap';

const Faq = React.createClass({
    getInitialState() {
      return {
        activeKey: '1',
      };
    },
  
    handleSelect(activeKey) {
      this.setState({ activeKey });
    },
  
    render() {
      return (
        <div className="container">
            <PanelGroup activeKey={this.state.activeKey} onSelect={this.handleSelect} accordion>
                <Panel style={cursor} header="What is Warshaplus?" eventKey="1">It's a company</Panel>
                <Panel style={cursor} header="How do I order?" eventKey="2">It's not difficult dude...just like any other websites.</Panel>
                <Panel style={cursor} header="..." eventKey="3">...</Panel>
                <Panel style={cursor} header="..." eventKey="4">...</Panel>
                <Panel style={cursor} header="..." eventKey="5">...</Panel>
                <Panel style={cursor} header="..." eventKey="6">...</Panel>
                <Panel style={cursor} header="..." eventKey="7">...</Panel>
                <Panel style={cursor} header="..." eventKey="8">...</Panel>
                <Panel style={cursor} header="..." eventKey="9">...</Panel>
            </PanelGroup>
        </div>
      );
    },
  });

  const cursor = {
      cursor: 'pointer'
  }

export default Faq;