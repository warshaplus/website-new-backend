import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { Button, Input } from 'semantic-ui-react';
import axios from 'axios';

import AuthService from '../auth/AuthService'

class Signup extends Component {
    constructor(props) {
        super(props);
      
        this.state = {
          form: {
              first_name: '', last_name: '', company: '', 
              email: '', password: '', mobile_no: '', 
              type: '', agrees: false
          },
          processing: false, 
          errMsg: ''
        };
  
        this.doRegister = this.doRegister.bind( this )
        this.handleInputChange = this.handleInputChange.bind( this )
        this.authService = new AuthService
      }
  
      componentWillMount() {
          this.authService.isAuthenticated() && this.props.history.push('/')
      }
  
      handleInputChange( event ) {
          const target = event.target;
          const value = target.type === 'checkbox' ? target.checked : target.value;
          // const value = target.value
          const name = target.name;
  
          var form = this.state.form 
          form[ name ] = value
  
          this.setState({
            form: form
          });
      }
  
      doRegister() {
          let form = this.state.form 
          
          // we are submitting now...
          this.setState({
              processing: true 
          });
  
          window.axios.post( '/register', form ).then( response => {
              let json = response.data
  
              if ( json.success ) {
                  // the registration was okay 
                  // save the user data in localStorage
                  this.authService.setUserData( json.user, json.token )
                  
                  // redirect to the intended page
                  const { from } = this.props.location.state || { from: { pathname: '/' } } 
                  this.props.history.push( from )
              } else {
                  this.setState({
                      processing: false, errMsg: json.details
                  });
              }
          }, error => {
              console.log( error )
              this.setState({
                  processing: false, 
                  errMsg: 'There was an error while processing your request'
              });
          })
      }

    render() {
        return (
            <div className="signup-dark">
                <form id="signup" method="post">
                    <div className="illustration">
                        <img src={require('../../../imgs/logo2.png')} />
                        
                    </div>
                    {/* First Name */}
                    <div className="form-group">
                        <input className="form-control" name="first_name" onChange={ this.handleInputChange } type="text" placeholder="First name" pattern="^[\w]{3,16}$" autoFocus required />
                    </div>
                    {/* Last Name */}
                    <div className="form-group">
                        <input className="form-control" name="last_name" onChange={ this.handleInputChange } type="text" placeholder="Last name" pattern="^[\w]{3,16}$" autoFocus required />
                    </div>
                    {/* Company Name */}
                    <div className="form-group">
                        <input className="form-control" name="company" onChange={ this.handleInputChange } type="text" placeholder="Company/corporation name" autoFocus required />
                    </div>
                    {/* Email */}
                    <div className="form-group">
                        <input className="form-control" name="email" onChange={ this.handleInputChange } type="email" placeholder="Email" />
                    </div>
                    {/* Password */}
                    <div className="form-group">
                        <input className="form-control" name="password" onChange={ this.handleInputChange } type="Password" placeholder="password" required />
                    </div>
                    {/* Phone */}
                    <div className="form-group">
                        <input className="form-control" name="mobile_no" onChange={ this.handleInputChange } id="mobile_no" type="tel" placeholder="Phone" maxLength="10" />
                    </div>
                        <center><h5>Are you a customer or a vendor?</h5></center>
                        <br />
                            <Col md={3} mdPush={1}>
                                <center>
                                    <label>
                                        Customer <br />
                                        <input name="type" value="customer" type="radio" onChange={ this.handleInputChange } />
                                    </label>
                                </center>
                            </Col>
                            <Col md={3} mdPush={5}>
                                <center>
                                    <label>
                                        Vendor <br />
                                        <input id="ifvendor" name="type" value="vendor" type="radio" onChange={ this.handleInputChange } />
                                    </label>
                                </center>
                            </Col>
                    <label>
                        <input name="agrees" type="checkbox" required value="terms" onChange={ this.handleInputChange } /> 
                        By registering, you agree to our <Link to="/terms" target="_blank"> Terms & conditions! </Link>
                    </label>
                    <div className="form-group">
                        <Button className="btn btn-primary btn-block" type="button" value="Register" color='orange' onClick={ this.doRegister }><h3> Register </h3></Button>
                    </div>
                    <center>
                        <Link to="/login">Have an account? Login</Link>
                    </center>
                </form>
            </div>
        );
    }
}
export default Signup;