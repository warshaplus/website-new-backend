import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import Header from '../header_footer/Header';
import Footer from '../header_footer/Footer';

import { Col } from 'react-bootstrap';
import { Button } from 'semantic-ui-react';
import { Link, Redirect, withRouter } from 'react-router-dom';
import axios from 'axios';

import AuthService from '../auth/AuthService';

class Login extends Component {
    constructor( props ) {
        super( props )
      
        this.state = {
          email: '', password: '', 
          processing: false, errMsg: '',
          valErrors: []
        }
  
        this.updateInput = this.updateInput.bind( this )
        this.doLogin = this.doLogin.bind( this )
  
        this.authService = new AuthService
      }
  
      componentWillMount() {
          this.authService.isAuthenticated() && this.props.history.push('/')
      }
  
      updateInput( event ) {
          this.setState({ [event.target.name]: event.target.value })
      }
  
      doLogin() {
          let email = this.state.email 
          let password = this.state.password 
  
          if ( ! email.length || ! password.length ) {
              this.setState({
                  errMsg: "Enter your email and password"
              });
  
              setTimeout(function() {
                  this.setState({
                      errMsg: ''
                  });
              }, 5000);
  
              return 
          }
  
          // form is valid 
          this.setState({
              processing: true 
          });
  
          window.axios.post( '/login', { email, password } ).then( response => {
              let json = response.data
  
              if ( json.success ) {
                  // the login was okay 
                  // save the user data in localStorage
                  this.authService.setUserData( json.user, json.token )
  
                  console.log( json.user, json.token )
  
                  // redirect to the intended page
                  const { from } = this.props.location.state || { from: { pathname: '/' } } 
                  this.props.history.push( from )
              } else {
                  this.setState({
                      processing: false, errMsg: json.details
                  });
              }
          }, error => {
              console.log( error )
              this.setState({
                  processing: false, 
                  errMsg: 'Are you sure this is the right info?'
              });
          })
      }

    
    render() {
        return (
            <div className="login-dark">
                <form id="login" method="post">
                    <div className="illustration">
                        <img src={require('../../../imgs/logo5.png')} width="250" />
                        <center><h5 style={{color: 'red'}}>{ this.state.errMsg.length && this.state.errMsg || '' }</h5></center>
                    </div>
                    <div className="form-group">
                        <input className="form-control" type="email" name="email" placeholder="Email" onChange={ this.updateInput } />
                    </div>
                    <div className="form-group">
                        <input className="form-control" type="password" name="password" placeholder="Password" onChange={ this.updateInput } />
                    </div>
                    <div className="form-group">
                        <button className="btn btn-primary btn-block" type="button" value="signin" onClick={ this.doLogin } >Log In</button>
                    </div>
                    <center>
                        <Link to="/register">create an account</Link> - <Link to="/forgot-password">forgot password</Link>
                    </center>
                </form>
            </div>
        );
    }
}
export default Login;