<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <title> Welcome to Warsha Plus </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <meta name="description" content="">
    <meta property="og:title" content=""/>
    <meta property="og:url" content="https://www.warshaplus.com"/>
    <meta property="og:description" content="">
    <meta property="og:image" content="">
    <meta property="og:type" content="article"/>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Semantic UI -->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.12/semantic.min.css"></link>

    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

    </body>
        <div id="root"></div>
        <script src={{asset('js/app.js')}}{{ '?b='.time() }}></script>
    </body>
</html>
